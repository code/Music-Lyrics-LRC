# NAME

Music::Lyrics::LRC - Manipulate LRC karaoke timed lyrics files

# VERSION

Version 0.17

# DESCRIPTION

Read, write, and do simple manipulations of the LRC lyrics files used for some
karaoke devices.

For details on the LRC file format, please see Wikipedia:

[https://en.wikipedia.org/wiki/LRC\_(file\_format)](https://en.wikipedia.org/wiki/LRC_\(file_format\))

# SYNOPSIS

    use Music::Lyrics::LRC;
    ...
    my $lrc = Music::Lyrics::LRC->new();
    open my $rfh, '<', 'mylyrics.lrc';
    $lrc->load($rfh);
    ...
    my $lyrics = $lrc->lyrics();  # arrayref of hashrefs: time (msec), text
    my $tags = $lrc->tags();  # hashref, name => value
    ...
    $lrc->add_lyric(5500, q(Now I'm singin' at exactly 5.5 seconds...));
    $lrc->set_tag('author', 'Justin A. Perlhacker');
    $lrc->unset_tag('author');
    ...
    open my $wfh, '>', 'savelyrics.lrc';
    $lrc->save($wfh);

# SUBROUTINES/METHODS

## `new(%opts)`

Constructor method. Accepts a hash with one attribute `verbose`. This
specifies whether the module will `warn` explicitly when it cannot parse an
input line from a file. It defaults to 0.

    my $lrc = Music::Lyrics::LRC->new();
    ...
    my $lrc_verbose = Music::Lyrics::LRC->new(verbose => 1);
    ...

## `lyrics()`

Retrieve an arrayref of hashrefs representing lyric lines, sorted by time
ascending. Each one has `time` and `text` keys. The time is in milliseconds.

    [
        {
            time => 5500,
            text => 'Now I\'m singin\' at exactly 5.5 seconds...',
        },
        {
            time => 6001,
            text => 'And now a moment after the sixth...',
        },
    ...
    ]

## `tags()`

Retrieve a hashref of tag names to tag values for this lyrics file.

    {
        ar => 'Justin A. Perlhacker',
        ti => 'Perl timekeeping blues',
        ...
    }

## `add_lyric($time, $text)`

Add a lyric at the given non-negative time in milliseconds and with the given
text. The text must not include newlines or carriage returns.

## `set_tag($name, $value)`

Set a tag with the given name and value. The name must be at least one
character and cannot have colons. Neither the name nor the value can include
newlines or carriage returns.

## `unset_tag($name)`

Clear a tag with the given name. Raises a warning if the tag has not been set.

## `load($fh)`

Load lyrics from the given readable filehandle.

## `save($fh)`

Save lyrics to the given writeable filehandle.

# DIAGNOSTICS

- `Bad lyric time`

    A lyric could not be added with the given time. It may have been negative.

- `Bad lyric line`

    The line you tried to add had illegal characters in it, probably a carriage
    return or a newline.

- `Bad tag name`

    The tag you tried to set had an illegal name. It needs to be at least one
    character, and can't include colons or whitespace.

- `Bad tag value`

    You tried to set a tag to an illegal value. The value probably had a carriage
    return or a newline in it.

- `Tag not set`

    You tried to clear a tag that had not already been set.

- `Unknown format for line %s`

    The parser ran across a line in the LRC file that it could not understand. It
    tolerates blank lines, tags, and lyric lines, and doesn't know anything else.

- `Failed file read: %s`

    The file read failed with the given system error.

- `Not a filehandle`

    You passed `load()` or `save()` something that wasn't a filehandle.

- `Failed tag write: %s`

    An attempt to write a tag to the output filehandle in `save()` failed with the
    given system error.

- `Failed lyric write: %s`

    An attempt to write a lyric timestamp and line to the output filehandle in
    `save()` failed with the given system error.

# CONFIGURATION AND ENVIRONMENT

You'll need to make sure that you're passing in a filehandle with the
appropriate I/O layers you want, especially encoding.

# DEPENDENCIES

- Perl 5.6 or newer
- [Carp](https://metacpan.org/pod/Carp)
- [English](https://metacpan.org/pod/English)

# INCOMPATIBILITIES

This module does not support any "extended" or "enhanced" LRC format; in
particular, at the time of writing it can't handle per-word times syntax. This
may change in future revisions.

# BUGS AND LIMITATIONS

The format accepted here is very liberal, and needs to be tested with lots of
different LRC files from the wild.

Fractional seconds of any length can be parsed, and preserved in the
millisecond count return by `lyrics()`, but any resolution beyond 2 decimal
places is lost on `save()`.

# AUTHOR

Tom Ryder `<tom@sanctum.geek.nz>`

# LICENSE AND COPYRIGHT

Copyright (C) 2017 Tom Ryder

This program is free software; you can redistribute it and/or modify it under
the terms of the Artistic License (2.0). You may obtain a copy of the full
license at:

[http://www.perlfoundation.org/artistic\_license\_2\_0](http://www.perlfoundation.org/artistic_license_2_0)

Any use, modification, and distribution of the Standard or Modified Versions is
governed by this Artistic License. By using, modifying or distributing the
Package, you accept this license. Do not use, modify, or distribute the
Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made by
someone other than you, you are nevertheless required to ensure that your
Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service mark,
tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge patent
license to make, have made, use, offer to sell, sell, import and otherwise
transfer the Package with respect to any patent claims licensable by the
Copyright Holder that are necessarily infringed by the Package. If you
institute patent litigation (including a cross-claim or counterclaim) against
any party alleging that the Package constitutes direct or contributory patent
infringement, then this Artistic License to you shall terminate on the date
that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER AND
CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES. THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY YOUR LOCAL LAW.
UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR CONTRIBUTOR WILL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING IN ANY WAY
OUT OF THE USE OF THE PACKAGE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
